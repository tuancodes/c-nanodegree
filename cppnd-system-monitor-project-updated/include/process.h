#ifndef PROCESS_H
#define PROCESS_H

#include <string>
/*
Basic class for Process representation
It contains relevant attributes as shown below
*/
class Process {
 public:
  int Pid();                               // TODO: See src/process.cpp
  std::string User();                      // TODO: See src/process.cpp
  std::string Command();                   // TODO: See src/process.cpp
  float CpuUtilization();                  // TODO: See src/process.cpp
  std::string Ram();                       // TODO: See src/process.cpp
  long int UpTime();                       // TODO: See src/process.cpp
  bool operator<(Process const& a) const;  // TODO: See src/process.cpp

  //
  void setPid(int pid);                          // TODO: See src/process.cpp
  void setUser(std::string user_);               // TODO: See src/process.cpp
  void setCommand(std::string command);          // TODO: See src/process.cpp
  void setCpuUtilization(float cpuUtilization);  // TODO: See src/process
  void setRam(std::string);                      // TODO: See src/process.cpp
  void setUpTime(long int upTime);               // TODO: See src/process.cpp
  // bool operator<(Process const& a) const;  // TODO: See src/process.cpp

  // TODO: Declare any necessary private members
 private:
  int pid;
  std::string user = "vpham";
  std::string command = "blank";
  float cpuUtilization = 22;
  std::string ram = "0";
  long int upTime = 10;
};

#endif