#include "processor.h"

#include <stdio.h>

#include <iostream>
#include <string>
#include <vector>

#include "linux_parser.h"

using std::cout;
using std::string;
using std::vector;

// TODO: Return the aggregate CPU utilization
float Processor::Utilization() {
  vector<string> cpuUsage = LinuxParser::CpuUtilization();
  // This takes the vector and aggregates it
  // print(cpuNums);

  // for (string n : cpuNums)
  //    cout << n << " \n";

  return stof(cpuUsage.back());
}