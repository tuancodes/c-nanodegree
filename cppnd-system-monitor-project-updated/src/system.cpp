#include "system.h"

#include <unistd.h>

#include <cstddef>
#include <iostream>
#include <set>
#include <string>
#include <vector>

#include "../include/linux_parser.h"
#include "process.h"
#include "processor.h"

using std::set;
using std::size_t;
using std::string;
using std::vector;

// TODO: Return the system's CPU
Processor& System::Cpu() { return cpu_; }

// TODO: Return a container composed of the system's processes
vector<Process>& System::Processes() {
  // vector<int> pids = LinuxParser::Pids();

  // for (int n : LinuxParser::Pids())
  //    std::cout << n << " \n";

  // LinuxParser::Uid(2875);
  processes_.clear();

  // string key;

 for (int pid : LinuxParser::Pids()) {
   Process* p1 = new Process();
   p1->setPid(pid);
   p1->setUser(LinuxParser::User(pid));
   p1->setCommand(LinuxParser::Command(pid));
   p1->setCpuUtilization(LinuxParser::CpuUtilization(pid));
   p1->setRam(LinuxParser::Ram(pid));
   p1->setUpTime(LinuxParser::UpTime(pid));

    string temp = LinuxParser::User(pid);

  // if(temp.compare("root") != 0) {
        processes_.push_back(*p1);
  // }

    //processes_.push_back(*p1);

   //delete p1;
 }
    // int pid = LinuxParser::Pids().front();
    // int pid2 = LinuxParser::Pids().back();

    // Process* p1 = new Process();
    // p1->setPid(pid);
    // p1->setUser(LinuxParser::User(pid));
    // p1->setCommand(LinuxParser::Command(pid));
    // p1->setCpuUtilization(999);
    // p1->setRam(LinuxParser::Ram(pid));
    // p1->setUpTime(5);

    // processes_.push_back(*p1);

    // Process* p2 = new Process();
    // p2->setPid(pid2);
    // p2->setUser(LinuxParser::User(pid2));
    // p2->setCommand(LinuxParser::Command(pid2));
    // p2->setCpuUtilization(999);
    // p2->setRam(LinuxParser::Ram(pid2));
    // p2->setUpTime(5);

    // processes_.push_back(*p2);








  // std::cout << "\np1.Command(): " << p1.Command() << "\n";

  // std::cout << "Inside System.cpp\n";
  // std::cout << "Size: " << processes_.size() << "\n";

  // string value;
  // float memTotal;

  return processes_;
}

// TODO: Return the system's kernel identifier (string)
std::string System::Kernel() { return LinuxParser::Kernel(); }

// TODO: Return the system's memory utilization
float System::MemoryUtilization() { return LinuxParser::MemoryUtilization(); }

// TODO: Return the operating system name
std::string System::OperatingSystem() { return LinuxParser::OperatingSystem(); }

// TODO: Return the number of processes actively running on the system
int System::RunningProcesses() { return LinuxParser::RunningProcesses(); }

// TODO: Return the total number of processes on the system
int System::TotalProcesses() { return LinuxParser::TotalProcesses(); }

// TODO: Return the number of seconds since the system started running
long int System::UpTime() { return LinuxParser::UpTime(); }