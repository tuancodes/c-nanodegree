#include "../include/linux_parser.h"

#include <dirent.h>
#include <unistd.h>

#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using std::stof;
using std::string;
using std::to_string;
using std::vector;

// DONE: An example of how to read data from the filesystem
string LinuxParser::OperatingSystem() {
  string line;
  string key;
  string value;
  std::ifstream filestream(kOSPath);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::replace(line.begin(), line.end(), ' ', '_');
      std::replace(line.begin(), line.end(), '=', ' ');
      std::replace(line.begin(), line.end(), '"', ' ');
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "PRETTY_NAME") {
          std::replace(value.begin(), value.end(), '_', ' ');
          return value;
        }
      }
    }
  }
  return value;
}

// DONE: An example of how to read data from the filesystem
string LinuxParser::Kernel() {
  string os, version, kernel;
  string line;
  std::ifstream stream(kProcDirectory + kVersionFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> os >> version >> kernel;
  }
  return kernel;
}

// BONUS: Update this to use std::filesystem
vector<int> LinuxParser::Pids() {
  vector<int> pids;
  DIR* directory = opendir(kProcDirectory.c_str());
  struct dirent* file;
  while ((file = readdir(directory)) != nullptr) {
    // Is this a directory?
    if (file->d_type == DT_DIR) {
      // Is every character of the name a digit?
      string filename(file->d_name);
      if (std::all_of(filename.begin(), filename.end(), isdigit)) {
        int pid = stoi(filename);
        pids.push_back(pid);
      }
    }
  }
  closedir(directory);
  return pids;
}

// TODO: Read and return the system memory utilization
float LinuxParser::MemoryUtilization() {
  string line;
  string key;
  string value;

  float memTotal;
  float memFree;

  std::ifstream filestream(kProcDirectory + kMeminfoFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "MemTotal:") {
          memTotal = stof(value);
        } else if (key == "MemFree:") {
          memFree = stof(value);
        }
      }
    }
  }

  return ((memTotal - memFree) / memTotal);
}

// TODO: Read and return the system uptime
long LinuxParser::UpTime() {
  string time;
  string line;
  std::ifstream stream(kProcDirectory + kUptimeFilename);

  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> time;
  }

  return stol(time);
}

// TODO: Read and return the number of jiffies for the system
long LinuxParser::Jiffies() { return 0; }

// TODO: Read and return the number of active jiffies for a PID
// REMOVE: [[maybe_unused]] once you define the function
long LinuxParser::ActiveJiffies(int pid [[maybe_unused]]) { return 0; }

// TODO: Read and return the number of active jiffies for the system
long LinuxParser::ActiveJiffies() { return 0; }

// TODO: Read and return the number of idle jiffies for the system
long LinuxParser::IdleJiffies() { return 0; }

// TODO: Read and return CPU utilization
vector<string> LinuxParser::CpuUtilization() {

  string line;
  string key;

  string prev_user;
  string prev_nice;
  string prev_system;
  string prev_idle;
  string prev_iowait;
  string prev_irq;
  string prev_softirq;
  string prev_steal;
  string prev_guest;
  string prev_guest_nice;

  string user;
  string nice;
  string system;
  string idle;
  string iowait;
  string irq;
  string softirq;
  string steal;
  string guest;
  string guest_nice;

  vector<string> oldCPUNums;
  vector<string> newCPUNums;
  vector<string> cpuUsage;

  std::ifstream filestream(kProcDirectory + kStatFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> prev_user >> prev_nice >> prev_system >>
             prev_idle >> prev_iowait >> prev_irq >> prev_softirq >>
             prev_steal >> prev_guest >> prev_guest_nice) {
        if (key == "cpu") {
          oldCPUNums.push_back(prev_user);
          oldCPUNums.push_back(prev_nice);
          oldCPUNums.push_back(prev_system);
          oldCPUNums.push_back(prev_idle);
          oldCPUNums.push_back(prev_iowait);
          oldCPUNums.push_back(prev_irq);
          oldCPUNums.push_back(prev_softirq);
          oldCPUNums.push_back(prev_steal);
          oldCPUNums.push_back(prev_guest);
          oldCPUNums.push_back(prev_guest_nice);
        }
      }
    }
  }

  filestream.close();

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  std::ifstream filestream2(kProcDirectory + kStatFilename);
  if (filestream2.is_open()) {
    while (std::getline(filestream2, line)) {
      std::istringstream linestream2(line);
      while (linestream2 >> key >> user >> nice >> system >> idle >> iowait >>
             irq >> softirq >> steal >> guest >> guest_nice) {
        if (key == "cpu") {
          newCPUNums.push_back(user);
          newCPUNums.push_back(nice);
          newCPUNums.push_back(system);
          newCPUNums.push_back(idle);
          newCPUNums.push_back(iowait);
          newCPUNums.push_back(irq);
          newCPUNums.push_back(softirq);
          newCPUNums.push_back(steal);
          newCPUNums.push_back(guest);
          newCPUNums.push_back(guest_nice);
        }
      }
    }
  }

  long PrevIdle = stol(prev_idle) + stol(prev_iowait);
  long Idle = stol(idle) + stol(iowait);
  long PrevNonIdle = stol(prev_user) + stol(prev_nice) + stol(prev_system) + stol(prev_irq) + stol(prev_softirq) + stol(prev_steal);
  long NonIdle = stol(user) + stol(nice) + stol(system) + stol(irq) + stol(softirq) + stol(steal);
  long PrevTotal = PrevIdle + PrevNonIdle;
  long Total = Idle + NonIdle;
  float totald = Total - PrevTotal;
  long idled = Idle - PrevIdle;

  float CPU_Percentage = (totald - idled) / totald;

  cpuUsage.push_back(to_string(CPU_Percentage));

  return cpuUsage;
}

// TODO: Read and return the total number of processes
int LinuxParser::TotalProcesses() {
  string line;
  string key;
  string value;

  long processes;

  std::ifstream filestream(kProcDirectory + kStatFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "processes") {
          processes = stol(value);
        }
      }
    }
  }

  return processes;
}

// TODO: Read and return the number of running processes
int LinuxParser::RunningProcesses() {
  string line;
  string key;
  string value;

  long runningProcesses;

  std::ifstream filestream(kProcDirectory + kStatFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "procs_running") {
          runningProcesses = stol(value);
        }
      }
    }
  }

  return runningProcesses;
}

// TODO: Read and return the command associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Command(int pid) {
  string line;
  string key;
  string value = "";

  std::ifstream filestream(kProcDirectory + to_string(pid) + kCmdlineFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key) {
        value = key;
      }
    }
  }

  return value;
}

// TODO: Read and return the memory used by a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Ram(int pid ) { 
  string line;
  string key;
  string value;
  string ram = "0";

  std::ifstream filestream(kProcDirectory + to_string(pid) + kStatusFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "VmSize:") {
          ram = value;
        }
      }
    }
  }
  return to_string((stoi(ram) / 1000));
}

// TODO: Read and return the user ID associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Uid(int pid) {
  string line;
  string key;
  string value;
  string uid;

  std::ifstream filestream(kProcDirectory + to_string(pid) + kStatusFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "Uid:") {
          uid = value;
        }
      }
    }
  }

  return uid;
}

// TODO: Read and return the user associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::User(int pid) {
  string line;
  string key;
  string value;
  string uid = LinuxParser::Uid(pid);
  string stringToBeFound = ":x:" + uid + ":" + uid + ":";
  string user = "blank";

  size_t colonPos;

  std::ifstream filestream2(kPasswordPath);
  if (filestream2.is_open()) {
    while (std::getline(filestream2, line)) {
      std::istringstream linestream2(line);
      while (linestream2 >> key) {
        size_t found = key.find(stringToBeFound);

        if (found != string::npos) {
          colonPos = key.find(":");
          user = key.substr(0, colonPos);
        }
      }
    }
  }

  return user;
}

// TODO: Read and return the uptime of a process
// REMOVE: [[maybe_unused]] once you define the function
long LinuxParser::UpTime(int pid) { 
  string line;
  string upTime;
  float upTimeReturn;
  int upTimePosition = 14;

  std::ifstream stream(kProcDirectory + to_string(pid) + kStatFilename);

  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    
    for (int pos = 0; pos < upTimePosition; pos++) {
      linestream >> upTime;
    }
  }
  

  //return stol(upTime) / sysconf(_SC_CLK_TCK);   

  upTimeReturn = stof(upTime) / sysconf(_SC_CLK_TCK); 

  return upTimeReturn;   
}

// TODO: Read and return the uptime of a process
// REMOVE: [[maybe_unused]] once you define the function
float LinuxParser::CpuUtilization(int pid) { 
  string line;
  string value;
  //string upTime = "0";
  string uTime = to_string(LinuxParser::UpTime(pid));
  string sTime;
  string cuTime;
  string csTime;
  string startTime;
  
  long sysUpTime = LinuxParser::UpTime();
  float totalTime;
  long seconds;
  long cpuUsage = 0;
  int uTimePosition = 14;
  int sTimePosition = 15;
  int cuTimePosition = 16;
  int csTimePosition = 17;
  int startTimePosition = 22;

  std::ifstream stream(kProcDirectory + to_string(pid) + kStatFilename);

  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    
    for (int pos = 0; pos < startTimePosition; pos++) {
      linestream >> value;

        switch(pos + 1) {
          case 15:
            sTime = value;
            break;
          case 16:
            cuTime = value;
            break;
          case 17:
            csTime = value;
            break;
          case 22:
            startTime = value;
            break;
        }
     
    }
  }

  totalTime = stoi(uTime) + stoi(sTime);
  totalTime = totalTime + stoi(cuTime) + stoi(csTime);
  seconds = stoi(uTime) - (stoi(startTime)/ sysconf(_SC_CLK_TCK));
  if (seconds > 0) {
    long ticks = sysconf(_SC_CLK_TCK);
    float t1 = totalTime / sysconf(_SC_CLK_TCK);
    float t2 = t1 / seconds;
    long t3 = t2 * 100;
    cpuUsage = 100 * ((totalTime / sysconf(_SC_CLK_TCK)) / seconds);
  }

  return cpuUsage;   
}