#include "format.h"

#include <string>

using std::string;

// TODO: Complete this helper function
// INPUT: Long int measuring seconds
// OUTPUT: HH:MM:SS
// REMOVE: [[maybe_unused]] once you define the function
string Format::ElapsedTime(long seconds) { 
    string hours_ = std::to_string(seconds / 3600);
    string minutes_ = std::to_string((seconds % 3600) / 60);
    string seconds_ = std::to_string(seconds % 60);
    
    if (hours_.length() == 1) {
        hours_ = "0" + hours_;
    }
    
    if (minutes_.length() == 1) {
        minutes_ = "0" + minutes_;
    }

    if (seconds_.length() == 1) {
        seconds_ = "0" + seconds_;
    }

    string timeDisplay = hours_ + ":" + minutes_ + ":" + seconds_;

    return timeDisplay; 
}