#include "../include/process.h"

#include <unistd.h>

#include <cctype>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::string;
using std::to_string;
using std::vector;

// TODO: Return this process's ID
int Process::Pid() { return pid; }

// TODO: Return this process's CPU utilization
float Process::CpuUtilization() { return cpuUtilization; }

// TODO: Return the command that generated this process
string Process::Command() { return command; }

// TODO: Return this process's memory utilization
string Process::Ram() { return ram; }

// TODO: Return the user (name) that generated this process
string Process::User() { return user; }

// TODO: Return the age of this process (in seconds)
long int Process::UpTime() { return upTime; }

// TODO: Overload the "less than" comparison operator for Process objects
// REMOVE: [[maybe_unused]] once you define the function
bool Process::operator<(Process const& a [[maybe_unused]]) const {
  return true;
}

void Process::setPid(int pid_) {  // TODO: See src/process.cpp
  this->pid = pid_;
}

void Process::setUser(std::string user_) { this->user = user_; }

void Process::setCommand(string command_) { this->command = command_; }

void Process::setCpuUtilization(float cpuUtilization_) {
  this->cpuUtilization = cpuUtilization_;
}

void Process::setRam(std::string ram_) { this->ram = ram_; }

void Process::setUpTime(long int upTime_) { this->upTime = upTime_; }